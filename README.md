# Project Neiio
A project to bring Neiio's themes, to Linux, namely for Cinnamon/GTK, and potentially GNOME (since Cinnamon & GNOME share the same code-base as each other), at first.

Contributions to Port Neiio's Themes to Linux are Welcome as Pull Requests (Fork this Repository, make your commits, and hit 'Pull Request' on that Repo, make it compare with this Repo, and send the request over to this repo).

<h2>Themes Pending Porting</h2>
List Coming Soon (Niivu, could you edit this part to add a list of all Neiio's themes please?)

<h2>Who is Neiio?</h2>
Neiio (Jamie Green) was a Windows Theme Designer that made beautiful themes for Windows, who sadly, soon after his famous port of Arc GTK to Windows, died last year. Niivu has been given the duty to keep the themes alive for Windows, and I thought it'd be awesome to bring Neiio's memory to a world that cares more about customisation, than Windows does (at the time of writing this).

<h2>Themes that won't get ported</h2>
The Themes that won't get ported will mainly be themes that were just ports of Linux Themes in the first place, including:

- Arc
